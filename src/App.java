import com.devcamp.s10.Author;
import com.devcamp.s10.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Victor Hugo", "victor@yahoo.com", 'm');
        Author author2 = new Author("Marie Joy", "marie@gmail.com", 'f');
        System.out.println(author1);
        System.out.println(author2);
        Book book1 = new Book("How to be good", author1, 120000, 5);
        Book book2 = new Book("Beauty and the beast", author2, 80000, 2);
        System.out.println(book1);
        System.out.println(book2);
    }
}
